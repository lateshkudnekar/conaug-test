import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ImageComponent} from "./image/image.component";
import { ListImagesComponent } from "./list-images/list-images.component";
import { SentenceCaseDirective } from "./sentence-case.directive";
@NgModule({
  declarations: [
    AppComponent,
    ImageComponent,
    ListImagesComponent,
    SentenceCaseDirective
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
