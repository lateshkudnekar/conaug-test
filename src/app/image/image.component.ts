
import { Component, OnInit, Renderer2, ElementRef, ViewChild, EventEmitter, Output } from '@angular/core';
import { ImageService } from '../image.service';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})

export class ImageComponent implements OnInit {

  error: string;
  uploadError: string;
  url;
  description=""
  uploadListArray:any = [];
  constructor(
    private imgService: ImageService,
  ) { }

  ngOnInit() {
    this.uploadListArray = [];
  }

  onSelectedFile(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
              var reader = new FileReader();
              if (!/\.(jpe?g|png|gif)$/i.test(event.target.files[0].name)){
                return alert(event.target.files[0].name +" is not an image");
              } // else...
              reader.onload = (event:any) => {
                 this.url=event.target.result; 
              }

              reader.readAsDataURL(event.target.files[i]);
      }
  }
  }


  async startUploading() {
      for(let i = 0; i<this.uploadListArray.length;i++ ) {
        
        this.uploadListArray[i].status="Uploading"
         await this.imgService.addImage({image:this.uploadListArray[i].image,description:this.uploadListArray[i].description})
          .subscribe((res)=> {
            this.uploadListArray[i].status="Uploaded"
          });
      }
  }

  timer() {
    return new Promise((reject,resolve) => {
      setTimeout(() => {
        resolve();
      }, 2000);
    })
  }

  addToUploadList() {
    if(this.url == '' && this.description=='') 
      return;

    this.uploadListArray.push({image:this.url, description: this.description, status:"Not uploaded"})
    this.url=''
    this.description=''
  }

  removeFromUploadList(item){
    let index = this.uploadListArray.indexOf(item);
    if (index > -1) {
      this.uploadListArray.splice(index, 1);
    }
  }

}

