import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  addImages: boolean;
  message: string;

  onAddImages() {
    this.addImages = true;
    this.message = '';
  }
  showListing() {
    this.addImages = false;
  }
}
