import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
  
import { Injectable } from '@angular/core';
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = "/api";

@Injectable({
  providedIn: 'root'
})
export class ImageService {


  constructor(private http: HttpClient) { }

  
  getImages(): Observable<any[]> {
    return this.http.get<any[]>(apiUrl)
      .pipe(
        tap(articles => console.log('fetched images')),
        catchError(this.handleError('an error ouccurred', []))
      );
  }

  
  addImage(imgObj): Observable<any> {
    return this.http.post<any>(apiUrl, imgObj, httpOptions).pipe(
      tap((obj: any) => console.log(`added Article w/ id=${obj._id}`)),
      catchError(this.handleError<any>('error in add image'))
    );
  }

  deleteImage(id: any): Observable<any> {
    const url = `${apiUrl}/${id}`;
    return this.http.delete<any>(url, httpOptions).pipe(
      tap(_ => console.log(`deleted Article id=${id}`)),
      catchError(this.handleError<any>('deleteArticle'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      return of(result as T);
    };
  }
}

