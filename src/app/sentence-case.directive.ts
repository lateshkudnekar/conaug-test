import { Directive, ElementRef, Renderer } from '@angular/core';

@Directive({
  selector: '[appSentenceCase]'
})
export class SentenceCaseDirective {

  constructor(private el: ElementRef, private renderer:Renderer) {

    renderer.setElementStyle(el.nativeElement, 'textTransform', 'capitalize');
   }
  
}
