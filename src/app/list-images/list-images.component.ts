import { Component, OnInit } from '@angular/core';
import {ImageService} from '../image.service'
@Component({
  selector: 'app-list-images',
  templateUrl: './list-images.component.html',
  styleUrls: ['./list-images.component.css']
})
export class ListImagesComponent implements OnInit {

  constructor(private imageService:ImageService) { }
  images:any = []
  ngOnInit() {
    this.imageService.getImages().subscribe((res) => {
      this.images = res
    })
  }

  deleteImage(image) {
    console.log(image)
    let index = this.images.indexOf(image);
    if (index > -1) {
      this.images.splice(index, 1);
    }
    this.imageService.deleteImage(image._id).subscribe((res) => {
      console.log("deleted")
    })
  }

}
