import mongoose, { Schema } from "mongoose";

const ImageSchema: Schema = new Schema({
    image: { type: String, required:true},
    description: {type: String, required:true},
    dateandtime: {type:Date, default: Date.now}
})

export default mongoose.model('Image', ImageSchema);