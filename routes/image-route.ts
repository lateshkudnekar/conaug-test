import { Request, Response, NextFunction } from "express";
import Image from "../models/images";

export class ImageRoute {
    public imageRoute(app): void {
        app.route('/api/').get((req:Request, res:Response, next:NextFunction) => {
            Image.find((err, images) => {
                if(err) { return next(err)}
                res.json(images)
            })
        })

        app.route('/api/:id').delete((req:Request, res: Response, next:NextFunction) => {
            Image.findByIdAndRemove(req.params.id, req.body, (err, image) => {
                if(err) { return next(err); }
                res.json(image)
            })
        })

        app.route('/api/').post((req: Request, res: Response, next: NextFunction) => {
            console.log(req.body);
            let imageData= req.body.image
         
            Image.create({image:imageData, description:req.body.description}, (err, resp) => {
              if (err) { return next(err); }
              res.json(resp);
            });
          });
      
    }
}